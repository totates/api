import logging from "external:@totates/logging/v1";
import { LocalizedError } from "external:@totates/translation/v1";

const logger = logging.getLogger("@totates/api/v1");

export class FetchError extends LocalizedError {
    constructor(response) {
        super(response.statusText);
    }
}

export class QuotquotBackendError extends LocalizedError {
    constructor(api, e, url) {
        super(e.service, e.error, e.data);
        this.service = e.service;
        this.error = e.error;
        this.url = url;
        this.api = api;
    }
}

/* {
    constructor(api, remote, url)
    {
        const namespace = remote.mixin ? `mixin-${remote.mixin}` : `service-${remote.service}`;
        super(`${namespace}:${remote.type} at ${url}: ${JSON.stringify(remote.data)}`);
        this.name = QuotquotBackendError.name;
        Error.captureStackTrace(this, QuotquotBackendError);
        this.remote = remote;
        this.url = url;
        this.api = api;
        this.key = `${namespace}:exception.${remote.type}`;
    }
}*/

export class API {
    constructor(service, version) {
        this.service = service;
        this.version = version;
        /** @todo allow other server */
        this.baseUrl = new window.URL(window.location);
        this.baseUrl.pathname = `/api/${service}/v${version}`;
        this.baseUrl.search = "";
        this.baseUrl.hash = "";
        logger.debug(`setting up api ${this.baseUrl.pathname}`);
    }

    post(path, params, headers) {
        let body;
        if (headers)
            body = params;
        else {
            headers = { "Content-Type": "application/json" };
            body = JSON.stringify(params);
        }

        return this.fetch(path, { method: "POST", headers, body });
    }

    async fetch(path, options = {}) {
        const url = new window.URL(this.baseUrl);
        url.pathname += path;

        if (!options.headers)
            options.headers = {};

        // options.headers['X-Token'] = await this.component.quotquotjs.getSessionToken();
        // options.mode = 'same-origin';

        const response = await window.fetch(url, options);

        if (!response.ok)
            throw new FetchError(response);

        const [success, result] = await response.json();

        if (success)
            return result;
        throw new QuotquotBackendError(this, result, url);
    }
}

// import user from "quotquotjs:service-user";

// TODO: same code in api component
async function _fetch(request, authenticate = true) {
    try {
        // await user._setAuthenticationHeader(request, authenticate);

        const response = await window.fetch(request);

        let result;
        const contentType = response.headers.get("Content-Type");

        if (contentType &&
            contentType.startsWith("application/") &&
            contentType.endsWith("json")) /* e.g. application/geo+json */
            result = await response.json();
        else
            logger.warn("unmanaged content type:", contentType);

        if (response.ok)
            return [true, result];

        if (result)
            result.type = "backend";
        else
            result = { type: "network" };

        result.status = response.status;
        return [false, result];
    }
    catch (exc) {
        logger.error(exc);
        return [false, { type: "unexpected", message: exc.message }];
    }
}

export default { fetch: _fetch };
